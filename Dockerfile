FROM node:8.15.0-alpine

LABEL maintainer="Ghlen Nagels <ghlen@pm.me>"

USER node

WORKDIR /home/node
RUN mkdir tmp app

WORKDIR tmp
COPY package.json  package-lock.jso[n] ./
RUN npm install --no-optional

WORKDIR ../app
COPY *.js package.json package-lock.json .babelrc ./

RUN npm install --no-optional \
 && rm -Rf ../tmp

ADD resources/ /home/node/app/resources/
ADD lib/ /home/node/app/lib
ADD .env/ /home/node/app/.env
ADD envloader.js /home/node/app/envloader.js
ADD webpack.common.js /home/node/app/webpack.common.js
ADD webpack.dev.js /home/node/app/webpack.dev.js
ADD webpack.hot.js /home/node/app/webpack.hot.js
ADD webpack.prod.js /home/node/app/webpack.prod.js

CMD ["sh", "-c", "npm run $APP_NPM_RUN_MODE"]
