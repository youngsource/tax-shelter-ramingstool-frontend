export const versions = {
    v2018aj: {
        description: "Aanslagjaar 2018",
        title : "2018",
        component: () => import('./components/Venb2018')
    },
    v2019ajOld: {
        description: "Aanslagjaar 2019 - oud",
        title : "Het belastbaar tijdperk vangt vóór 01.01.2018 aan",
        component: () => import('./components/Venb2019Old')
    },
    v2019ajNew: {
        description: "Aanslagjaar 2019 - nieuw",
        title : "Het belastbaar tijdperk vangt ten vroegste op 01.01.2018 aan",
        component: () => import('./components/Venb2019New')
    }
};

/**
 * Returns the latest version.
 * @returns {number}
 */
export function latestVersion() {
    return 'v2019ajNew'
}

/**
 * Registers the venb version components to the given Vue instance.
 * @param Vue
 */
export default function registerVenbVersions(Vue) {
    for (let [key, value] of Object.entries(versions)) {
        Vue.component(key, value.component)
    }
}
