import auth from '../../lib/auth/auth';

export default [
    {
        name: 'taxshelter',
        path: '/taxshelter',
        display: 'tax shelter',
        component: () => import('../../lib/tax-shelter/resources/js/components/TaxShelter'),
        meta: {
            middleware: auth
        },
    },
    {
        name: "venb",
        path: "/venb",
        display: 'venb',
        component: () => import('./components/Venb')
    }
]
