import '@babel/polyfill';
import 'es6-promise';
import Vue from 'vue';
import VueAxios from 'vue-axios';
import axios from 'axios';
import VueRouter from 'vue-router';
import App from './components/App';
import router from './router';
import EventBus from './event-bus';
import 'bootstrap';
import VueI18n from 'vue-i18n';
import translations from '../../lib/tax-shelter/resources/lang/all';
import registerVenbVersions from './venb-versions';

axios.defaults.baseURL = process.env.API_URL;

const token = localStorage.getItem('token');
if (token !== 'undefined' && token !== undefined && token !== null && token !== 'null') {
    axios.defaults.headers.common.Authorization = token;
}

axios.interceptors.response.use(config => config, (error) => {
    console.log(error.response);
    if (error.response && error.response.status === 401) {
        delete axios.defaults.headers.common.Authorization;
        localStorage.removeItem('token');
        router.push({name: 'login'});
        return Promise.reject(error);
    }
    return Promise.reject(error);
});

Vue.use(VueAxios, axios);

Vue.use(VueRouter);

Vue.use(VueI18n);

Vue.prototype.$bus = EventBus;

const lang = localStorage.getItem('lang');
if (lang === null) {
    localStorage.setItem('lang', 'nl');
}
const i18n = new VueI18n({
    locale: lang === null ? 'nl' : lang,
    messages: translations
});

registerVenbVersions(Vue);

new Vue({
    router: router,
    render: h => h(App),
    i18n: i18n,
    envAppName: process.env.APP_NAME
}).$mount('#app');

