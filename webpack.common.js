require('./envloader');
const path = require('path');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const ManifestPlugin = require('webpack-manifest-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackHarddiskPlugin = require('html-webpack-harddisk-plugin');
const DotEnvWebpackPlugin = require('dotenv-webpack');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const BundleAnalyserPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const webpack = require('webpack');

module.exports = {
    name: 'calculators-webpack',
    entry: {
        app: ['./resources/js/app.js', './resources/styling/scss/app.scss'],
    },
    output: {
        publicPath: '/dist/',
        path: path.resolve(__dirname, 'public/dist/'),
        filename: '[name].bundle.js',
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: [ /node_modules/, /nova-components/ ],
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: [
                            '@babel/preset-env'
                        ],
                        plugins: [
                            '@babel/plugin-syntax-dynamic-import',
                            '@babel/plugin-proposal-object-rest-spread',
                            '@babel/plugin-transform-runtime',
                            [
                                '@babel/plugin-transform-arrow-functions',
                                {
                                    'spec': true
                                }
                            ],
                            '@babel/plugin-proposal-class-properties'
                        ]
                    }
                }
            },
            {
                test: /\.vue$/,
                loader: [
                    'vue-loader',
                    // 'eslint-loader'
                ],
            },
            {
                test: /\.scss$/,
                use: [
                    process.env.NODE_ENV !== 'production' ? 'vue-style-loader' : MiniCssExtractPlugin.loader,
                    {
                        loader: 'css-loader',
                    },
                    {
                        loader: 'resolve-url-loader',
                        options: {
                            keepQuery: true,
                            debug: false,
                        },
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            data: '@import "../../../lib/tax-shelter/resources/styling/scss/' + process.env.APP_NAME + '";',
                            outFile: 'assets/styling/sass/app.scss',
                            sourceMap: true,
                            sourceMapContents: false,
                        },
                    },
                ],
            },
            {
                test: /\.css$/,
                use: [
                    process.env.NODE_ENV !== 'production' ? 'vue-style-loader' : MiniCssExtractPlugin.loader,
                    'css-loader',
                ],
            },
            {
                test: /\.(png|ico|svg|jpg|gif|ttf|woff(2)?|eot)(\?[\w\d#=&]+)?$/,
                use: [
                    {
                        loader: 'file-loader',
                    },
                ],
            },
        ],
    },
    plugins: [
        new VueLoaderPlugin(),
        new ManifestPlugin({
            fileName: 'mix-manifest.json',
            publicPath: '/',
            basePath: '/',
            writeToFileEmit: true,
        }),
        new CleanWebpackPlugin([
            'public/dist',
            'public/index.html'
        ], {
            watch: true,
            verbose: true,
        }),
        new HtmlWebpackPlugin({
            title:  'rekentools',
            filename: '../index.html',
            template: 'lib/common/resources/pages/index.html',
            alwaysWriteToDisk: true,
            favicon: "resources/assets/favicons/favicon.ico"
        }),
        new HtmlWebpackHarddiskPlugin(),
        new DotEnvWebpackPlugin(),
        new CopyWebpackPlugin([
            {from: path.resolve(__dirname, 'lib/common/resources/assets'), to:  path.resolve(__dirname, 'public/images')}
        ]),
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery"
        }),
        new BundleAnalyserPlugin()
    ],
    resolve: {
        extensions: ['.vue', '.js', '.scss', '.css'],
        alias: {
            vue: 'vue/dist/vue.js',
            jquery: "jquery/src/jquery"
        }
    }
};
